<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class WelcomeController extends Controller
{
      public function index(){

      //    return view('welcome');

          $citas = $this->marketService->getCitas();

          return response()->json([
              'citas' => $citas,
          ], 200);

      }

}
