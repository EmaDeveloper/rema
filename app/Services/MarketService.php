<?php

namespace App\Services;
use App\Traits\AuthorizesMarketResquests;
use App\Traits\ConsumesExternalServices;
use App\Traits\InteractsWithMarketResponses;

class MarketService
{
    use ConsumesExternalServices, AuthorizesMarketResquests, InteractsWithMarketResponses;

    // URL base a utilizar
    protected $baseUri;

    public function __construct()
    {
        $this->baseUri = config('services.market.base_uri');
    }

    public function getCitas()
    {
      return $this->makeRequest('GET','api/citas');
    }

}
