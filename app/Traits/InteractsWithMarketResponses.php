<?php

namespace App\Traits;

trait InteractsWithMarketResponses
{
  // Decodificar la respuesta
    public function decodeResponse($response)
    {

        $decodedResponse = json_decode($response);
        return $decodedResponse->data ?? $decodedResponse;
    }

    // Resuelve si hay algun error en la peticion
    public function checkIfErrorResponse($response)
    {
        if (isset($response->error)) {
            throw new \Exception("Error Processing Request: {$response->error}");
        }
    }
}
