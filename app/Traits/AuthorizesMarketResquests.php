<?php

namespace App\Traits;

use App\Services\MarketAuthenticationService;

trait AuthorizesMarketResquests
{

      /*
      Resolver los elementos necesarios al momento de revisar la autorizacion
      */
      public function resolveAuthorization(&$queryParams, &$formParams, &$headers)
      {
         $accessToken = $this->resolveAccessToken();
         $headers['Authorization'] = $accessToken;
      }


      public function resolveAccessToken()
      {

          $authenticationService = resolve(MarketAuthenticationService::class);
          return $authenticationService->getClientCredentialsToken();

      }
}
